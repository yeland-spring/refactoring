package com.twu.refactoring;

import java.util.List;

public class Order {
    private String name;
    private String address;
    private List<LineItem> lineItems;

    public Order(String name, String address, List<LineItem> lineItems) {
        this.name = name;
        this.address = address;
        this.lineItems = lineItems;
    }

    String getCustomerName() {
        return name;
    }

    String getCustomerAddress() {
        return address;
    }

    private List<LineItem> getLineItems() {
        return lineItems;
    }

    StringBuilder printLineItems() {
        StringBuilder lineItems = new StringBuilder();
        getLineItems().forEach(item ->
            lineItems.append(item.getDescription()).append('\t')
                .append(item.getPrice()).append('\t')
                .append(item.getQuantity()).append('\t')
                .append(item.totalAmount()).append('\n')
        );
        return lineItems;
    }

    double getTotalSalesTax() {
        double rate = .10;
        return lineItems.stream().mapToDouble(item -> item.totalAmount()*rate).sum();
    }

    double getTotal() {
        double total = lineItems.stream().mapToDouble(LineItem::totalAmount).sum();
        return total + getTotalSalesTax();
    }
}
