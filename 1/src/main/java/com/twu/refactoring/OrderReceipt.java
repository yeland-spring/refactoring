package com.twu.refactoring;

public class OrderReceipt {
    private Order order;

    public OrderReceipt(Order order) {
        this.order = order;
    }

    public String printReceipt() {
        String header = "======Printing Orders======\n";
        String salesTax = "Sales Tax";
        String totalAmount = "Total Amount";
        StringBuilder output = new StringBuilder();

        output.append(header);
        output.append(order.getCustomerName());
        output.append(order.getCustomerAddress());
        output.append(order.printLineItems());
        output.append(salesTax).append('\t').append(order.getTotalSalesTax());
        output.append(totalAmount).append('\t').append(order.getTotal());
        return output.toString();
    }
}