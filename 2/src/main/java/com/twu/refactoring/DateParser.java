package com.twu.refactoring;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

public class DateParser {
    private final String dateAndTimeString;
    private static final HashMap<String, TimeZone> KNOWN_TIME_ZONES = new HashMap<String, TimeZone>();

    static {
        KNOWN_TIME_ZONES.put("UTC", TimeZone.getTimeZone("UTC"));
    }

    public DateParser(String dateAndTimeString) {
        this.dateAndTimeString = dateAndTimeString;
    }

    public Date parse() {
        int year, month, date, hour, minute;

        year = getTime(0, 4, "Year string is less than 4 characters", "Year is not an integer");
        checkBounds(year, 2000, 2012, "Year cannot be less than 2000 or more than 2012");

        month = getTime(5, 7, "Month string is less than 2 characters", "Month is not an integer");
        checkBounds(month, 1, 12, "Month cannot be less than 1 or more than 12");

        date = getTime(8, 10, "Date string is less than 2 characters", "Date is not an integer");
        checkBounds(date, 1, 31, "Date cannot be less than 1 or more than 31");

        hour = getTimeOfZone(11, 13, "Hour string is less than 2 characters", "Hour is not an integer", 23, "Hour cannot be less than 0 or more than 23");
        minute = getTimeOfZone(14, 16, "Minute string is less than 2 characters", "Minute is not an integer", 59, "Minute cannot be less than 0 or more than 59");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeZone(TimeZone.getTimeZone("UTC"));
        calendar.set(year, month - 1, date, hour, minute, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    private int getTimeOfZone(int begin, int end, String lengthMessage, String typeMessage, int max, String numberMessage) {
        int hour;
        if (dateAndTimeString.substring(11, 12).equals("Z")) {
            hour = 0;
        } else {
            hour = getTime(begin, end, lengthMessage, typeMessage);
            checkBounds(hour, 0, max, numberMessage);
        }
        return hour;
    }

    private void checkBounds(int time, int min, int max, String message) {
        if (time < min || time > max)
            throw new IllegalArgumentException(message);
    }

    private int getTime(int begin, int end, String lengthMessage, String typeMessage) {
        int time;
        try {
            String yearString = dateAndTimeString.substring(begin, end);
            time = Integer.parseInt(yearString);
        } catch (StringIndexOutOfBoundsException e) {
            throw new IllegalArgumentException(lengthMessage);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(typeMessage);
        }
        return time;
    }
}
